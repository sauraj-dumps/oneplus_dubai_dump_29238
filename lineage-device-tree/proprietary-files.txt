# Unpinned blobs from OnePlusTV-user 11 RTM1.210315.027 2303030058 release-keys

# Audio
vendor/lib/hw/android.hardware.audio.effect@6.0-impl.so
vendor/lib/hw/android.hardware.audio@6.0-impl.so
vendor/lib/hw/audio.primary.default.so
vendor/lib/hw/audio.primary.mt5867.so
vendor/lib/hw/audio.r_submix.default.so
vendor/lib/hw/audio.usb.default.so
vendor/lib/libalsautils.so
vendor/lib/libaudioparser.so
vendor/lib/libiniparser.so
vendor/lib/libmi3.so
vendor/lib/libmi3_client.so
vendor/lib/libmtkrm_hwbinder_vendor.so
vendor/lib/libmutils.so
vendor/lib/libteec.so
vendor/lib/libutopia.so
vendor/lib/vendor.dolby.ms12@1.0.so
vendor/lib/vendor.mediatek.tv.mtkrm@1.0_vendor.so

# Audio (FX modules)
vendor/lib/soundfx/libdynproc.so

# Audio configs
etc/audio_effects.conf
vendor/etc/audio_effects.xml
vendor/etc/audio_policy_configuration.xml
vendor/etc/audio_policy_engine_configuration.xml
vendor/etc/audio_policy_engine_criteria.xml
vendor/etc/audio_policy_engine_criterion_types.xml
vendor/etc/audio_policy_engine_default_stream_volumes.xml
vendor/etc/audio_policy_engine_product_strategies.xml
vendor/etc/audio_policy_engine_stream_volumes.xml
vendor/etc/audio_policy_volumes.xml
vendor/etc/bluetooth_audio_policy_configuration.xml
vendor/etc/default_volume_tables.xml
vendor/etc/r_submix_audio_policy_configuration.xml
vendor/etc/usb_audio_policy_configuration.xml
vendor/etc/voiceprocess_audio_policy_configuration.xml

# Bluetooth
vendor/bin/hw/android.hardware.bluetooth@1.0-service
vendor/etc/init/android.hardware.bluetooth@1.0-service.rc
vendor/lib/hw/android.hardware.bluetooth@1.0-impl.so

# Bluetooth (A2DP)
vendor/lib/hw/android.hardware.bluetooth.audio@2.0-impl.so
vendor/lib/hw/audio.bluetooth.default.so
vendor/lib/libbluetooth_audio_session.so

# Boot
vendor/bin/hw/android.hardware.boot@1.1-service
vendor/etc/init/android.hardware.boot@1.1-service.rc
vendor/etc/vintf/manifest/android.hardware.boot@1.1.xml

# Camera
vendor/lib/hw/android.hardware.camera.provider@2.4-impl.so
vendor/lib/camera.device@1.0-impl.so
vendor/lib/camera.device@3.2-impl.so
vendor/lib/camera.device@3.3-impl.so
vendor/lib/camera.device@3.4-external-impl.so
vendor/lib/camera.device@3.4-impl.so
vendor/lib/camera.device@3.5-external-impl.so
vendor/lib/camera.device@3.5-impl.so
vendor/lib/camera.device@3.6-external-impl.so

# CAS
vendor/bin/hw/android.hardware.cas@1.2-service
vendor/etc/init/android.hardware.cas@1.2-service.rc
vendor/etc/vintf/manifest/android.hardware.cas@1.2-service.xml

# Display
vendor/bin/hw/android.hardware.graphics.allocator@4.0-service
vendor/bin/hw/android.hardware.graphics.composer@2.4-service
vendor/bin/hw/android.hardware.memtrack@1.0-service
vendor/etc/init/android.hardware.graphics.allocator@4.0-service.rc
vendor/etc/init/android.hardware.graphics.composer@2.4-service.rc
vendor/etc/init/android.hardware.memtrack@1.0-service.rc
vendor/lib/egl/libGLES_mali.so
vendor/lib/hw/android.hardware.memtrack@1.0-impl.so
vendor/lib/hw/gralloc.default.so
vendor/lib/hw/hwcomposer.mt5867.so
vendor/lib/hw/memtrack.mt5867.so
vendor/lib/hw/vulkan.mt5867.so
vendor/lib/arm.graphics-V1-ndk_platform.so
vendor/lib/libMtkRmClient.so
vendor/lib/libapp_if.so
vendor/lib/libapp_if_rpc.so
vendor/lib/libcmpb.so
vendor/lib/libdtv_common.so
vendor/lib/libdtv_osai.so
vendor/lib/libfbdev.so
vendor/lib/libhandle_app.so
vendor/lib/libhwc2onfbadapter.so
vendor/lib/libmmdisp.so
vendor/lib/libmtal.so
vendor/lib/libmtal_custom.so
vendor/lib/libnetmagic.so
vendor/lib/librpc_ipc.so
vendor/lib/libstagefrighthw_yuv.so
vendor/lib/libvsyncbridge.so
vendor/lib/mstar.hardware.mmdisp@1.0.so
vendor/lib/vendor.mediatek.graphics.externalbuffer@1.0.so

# DRM
vendor/bin/hw/android.hardware.drm@1.3-service.clearkey
vendor/bin/hw/android.hardware.drm@1.3-service.netflixdrm
vendor/bin/hw/android.hardware.drm@1.3-service.playready
vendor/bin/hw/android.hardware.drm@1.3-service.widevine
vendor/etc/init/android.hardware.drm@1.3-service.clearkey.rc
vendor/etc/init/android.hardware.drm@1.3-service.netflixdrm.rc
vendor/etc/init/android.hardware.drm@1.3-service.playready.rc
vendor/etc/init/android.hardware.drm@1.3-service.widevine.rc
vendor/etc/vintf/manifest/manifest_android.hardware.drm@1.3-service.clearkey.xml
vendor/etc/vintf/manifest/manifest_android.hardware.drm@1.3-service.widevine.xml
vendor/lib/mediacas/libclearkeycasplugin.so
vendor/lib/mediadrm/libdrmclearkeyplugin.so
vendor/lib/mediadrm/libnetflixdrmhidl.so
vendor/lib/mediadrm/libnetflixmediadrmplugin.so
vendor/lib/mediadrm/libwvdrmengine.so
vendor/lib/liboemcrypto.so
vendor/lib/libwvhidl.so

# Gatekeeper
vendor/bin/hw/android.hardware.gatekeeper@1.0-service
vendor/etc/init/android.hardware.gatekeeper@1.0-service.rc
vendor/lib/hw/android.hardware.gatekeeper@1.0-impl.so
vendor/lib/hw/gatekeeper.mt5867.so

# Health
vendor/bin/hw/android.hardware.health@2.1-service
vendor/etc/init/android.hardware.health@2.1-service.rc
vendor/etc/vintf/manifest/android.hardware.health@2.1.xml

# Keymaster
vendor/bin/hw/android.hardware.keymaster@4.0-service.tee
vendor/etc/init/android.hardware.keymaster@4.0-service.tee.rc

# Light
vendor/bin/hw/android.hardware.lights-service.mediatek

# Local time
vendor/lib/hw/local_time.default.so

# Media (Codec2)
vendor/bin/hw/android.hardware.media.c2@1.0-mediatek
vendor/etc/init/android.hardware.media.c2@1.0-mediatek.rc
vendor/etc/vintf/manifest/android.hardware.media.c2@1.0-mediatek.xml

# Media (OMX)
apex/com.android.media.swcodec/lib/android.hardware.media.bufferpool@2.0.so
etc/seccomp_policy/mediacodec.policy
lib/libmedia_codeclist.so
lib/libstagefright_bufferpool@2.0.1.so
lib/libstagefright_codecbase.so
lib/libstagefright_framecapture_utils.so
system_ext/lib/libmediandk.mtk.so
system_ext/lib/libstagefright_utils.mtk.so
system_ext/lib/vendor.mediatek.tv.mtkmtalservice@1.0.so
vendor/bin/hw/android.hardware.media.omx@1.0-service
vendor/etc/init/android.hardware.media.omx@1.0-service.rc
vendor/etc/seccomp_policy/mediacodec.policy
vendor/lib/libstagefright_bufferpool@2.0.1.so
vendor/lib/libstagefright_softomx_plugin.so
vendor/lib/libstagefrighthw.so

# Media configs
vendor/etc/media_codecs.xml
vendor/etc/media_codecs_c2.xml
vendor/etc/media_codecs_google_audio.xml
vendor/etc/media_codecs_google_video_le.xml
vendor/etc/media_codecs_performance.xml
vendor/etc/media_codecs_performance_c2.xml
vendor/etc/media_profiles_V1_0.xml

# Thermal
vendor/bin/hw/android.hardware.thermal@2.0-service.mediatek
vendor/etc/init/android.hardware.thermal@2.0-service.mediatek.rc
vendor/etc/vintf/manifest/android.hardware.thermal@2.0-service.mediatek.xml

# TV
vendor/bin/hw/android.hardware.tv.cec@1.0-service
vendor/bin/hw/android.hardware.tv.input@1.0-service-mediatek
vendor/etc/init/android.hardware.tv.cec@1.0-service.rc
vendor/etc/init/android.hardware.tv.input@1.0-service-mediatek.rc
vendor/lib/hw/android.hardware.tv.cec@1.0-impl.so
vendor/lib/hw/tv_input.default.so
vendor/lib/hw/tv_input.mt5867.so
vendor/lib/libmtkcutils.so

# Wi-Fi
vendor/bin/hw/android.hardware.wifi@1.0-service-lazy
vendor/bin/hw/hostapd
vendor/bin/hw/wpa_supplicant
vendor/etc/init/android.hardware.wifi@1.0-service-lazy.rc
vendor/etc/vintf/manifest/android.hardware.wifi.hostapd.xml
vendor/etc/vintf/manifest/android.hardware.wifi@1.0-service.xml
vendor/lib/libkeystore-engine-wifi-hidl.so
vendor/lib/libkeystore-wifi-hidl.so
vendor/lib/libwifi-hal.so
vendor/lib/vendor.mediatek.hardware.wifi.supplicant@1.0.so

# Wi-Fi configs
vendor/etc/wifi/p2p_supplicant.conf
vendor/etc/wifi/wifi.cfg
vendor/etc/wifi/wpa_supplicant.conf

# Miscellaneous
odm/app/TvLauncherCustomizer2/TvLauncherCustomizer2.apk
vendor/app/DolbyAudioService/DolbyAudioService.apk
vendor/bin/hw/mediatek.hardware.power-service
vendor/bin/hw/mstar.hardware.audio.service
vendor/bin/hw/mstar.hardware.mmdisp@1.0-service
vendor/bin/hw/vendor.mediatek.camera.provider@2.4-external-service
vendor/bin/hw/vendor.mediatek.graphics.externalbuffer@1.0-service
vendor/bin/hw/vendor.mediatek.hardware.tv.networkproxy@1.0-service
vendor/bin/hw/vendor.mediatek.hardware.wifidisplay@1.0-service
vendor/bin/hw/vendor.mediatek.tv.OneplusTvApi@1.0-service
vendor/bin/hw/vendor.mediatek.tv.cmpb@1.0-service
vendor/bin/hw/vendor.mediatek.tv.mtkdmservice@1.0-service
vendor/bin/hw/vendor.mediatek.tv.mtkmtalservice@1.0-service
vendor/bin/hw/vendor.mediatek.tv.mtkrm@1.0-service
vendor/bin/hw/vendor.mediatek.tv.mtktvapi@1.0-service
vendor/bin/hw/vendor.mediatek.tv.mtktvfactory@1.0-service
vendor/bin/MtkRmInitServer
vendor/bin/MtkRmServer
vendor/bin/applypatch
vendor/bin/boots
vendor/bin/boots_srv
vendor/bin/boringssl_self_test32
vendor/bin/capture
vendor/bin/chattr
vendor/bin/cusstUT
vendor/bin/devmem
vendor/bin/dumpsys
vendor/bin/fast_connect
vendor/bin/fsync
vendor/bin/getconf
vendor/bin/hdcp_mt
vendor/bin/i2cdetect
vendor/bin/i2cdump
vendor/bin/i2cget
vendor/bin/i2cset
vendor/bin/iconv
vendor/bin/install
vendor/bin/intertaca
vendor/bin/intertaca_manual
vendor/bin/iwpriv
vendor/bin/k2logcat
vendor/bin/linker
vendor/bin/lsattr
vendor/bin/make_dir_symlink
vendor/bin/make_dtb_img
vendor/bin/make_dtbo_img
vendor/bin/mksh
vendor/bin/nc
vendor/bin/netcat
vendor/bin/netflixdrmtest
vendor/bin/nproc
vendor/bin/nsenter
vendor/bin/oemcrypto_test
vendor/bin/picus
vendor/bin/playreadymediadrmplugintest
vendor/bin/readelf
vendor/bin/set_wopacket_port
vendor/bin/tee-supplicant
vendor/bin/test
vendor/bin/trigOAD_fusion
vendor/bin/unlink
vendor/bin/unshare
vendor/bin/update_linux_network
vendor/bin/usb_otg_agent
vendor/bin/uuidgen
vendor/bin/watch
vendor/bin/wlan_dongle_detect
vendor/bin/xtest
vendor/etc/bluetooth/bt_addr.conf
vendor/etc/init/boringssl_self_test.rc
vendor/etc/init/cpu_audio.rc
vendor/etc/init/dtv_svc.rc
vendor/etc/init/hostapd.android.rc
vendor/etc/init/init.fusion.rc
vendor/etc/init/lights-mtk-default.rc
vendor/etc/init/midaemon.rc
vendor/etc/init/mstar.hardware.audio.service.rc
vendor/etc/init/mstarmmdisp_service.rc
vendor/etc/init/mtk_rm.rc
vendor/etc/init/power-mediatek.rc
vendor/etc/init/vendor.mediatek.camera.provider@2.4-external-service.rc
vendor/etc/init/vendor.mediatek.graphics.externalbuffer@1.0-service.rc
vendor/etc/init/vendor.mediatek.hardware.tv.networkproxy@1.0-service.rc
vendor/etc/init/vendor.mediatek.hardware.wifidisplay@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.OneplusTvApi@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.cmpb@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.mtkdmservice@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.mtkmtalservice@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.mtkrm@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.mtktvapi@1.0-service.rc
vendor/etc/init/vendor.mediatek.tv.mtktvfactory@1.0-service.rc
vendor/etc/init/vendor_flash_recovery.rc
vendor/etc/init/vndservicemanager.rc
vendor/etc/parameter-framework/Settings/Policy/PolicyConfigurableDomains.xml
vendor/etc/parameter-framework/Structure/Policy/PolicyClass.xml
vendor/etc/parameter-framework/Structure/Policy/PolicySubsystem-CommonTypes.xml
vendor/etc/parameter-framework/Structure/Policy/PolicySubsystem.xml
vendor/etc/parameter-framework/Structure/Policy/ProductStrategies.xml
vendor/etc/parameter-framework/ParameterFrameworkConfigurationPolicy.xml
vendor/etc/ppp/options.conf
vendor/etc/seccomp_policy/mediaextractor.policy
vendor/etc/service/pre-init.sh
vendor/etc/vintf/manifest/lights-mtk-default.xml
vendor/etc/vintf/manifest/manifest.xml
vendor/etc/vintf/manifest/power-mediatek.xml
vendor/etc/vintf/manifest/vendor.mediatek.camera.provider.xml
vendor/etc/VendorPlayer.cfg
vendor/etc/adb_enable_off.ini
vendor/etc/adb_enable_on.ini
vendor/etc/an_nfx_audio_cap.ini
vendor/etc/audio_policy.conf
vendor/etc/boot.conf
vendor/etc/external_camera_config.xml
vendor/etc/fstab.mt5867
vendor/etc/fstab_late.mt5867
vendor/etc/global_settings.txt
vendor/etc/hwcomposer13.ini
vendor/etc/mkshrc
vendor/etc/public.libraries.txt
vendor/etc/rc.local
vendor/etc/rc.local.partial
vendor/etc/set_env
vendor/etc/surround_sound_configuration_5_0.xml
vendor/etc/tv-settings-configs.xml
vendor/etc/tv_satellite_config.xml
vendor/etc/utopia.conf
vendor/factory/factory_clt
vendor/firmware/ARTL8752CRF-1510-3759_OTA_V2.3.4870.220905.bin
vendor/firmware/ARTL8752CRF-1515-4263_OTA_V3.0.4872.220905.bin
vendor/firmware/ARTL8752CRF-1515-4656_OTA_V3.0.4873.220905.bin
vendor/firmware/ARTL8752CRF-1712-5228_OTA_V1.8.4875.221208.bin
vendor/firmware/EEPROM_MT7663.bin
vendor/firmware/EEPROM_MT7668.bin
vendor/firmware/TxPwrLimit_MT76x3.dat
vendor/firmware/TxPwrLimit_MT76x8.dat
vendor/firmware/WIFI_RAM_CODE2_USB_MT7668.bin
vendor/firmware/WIFI_RAM_CODE_MT7663.bin
vendor/firmware/WIFI_RAM_CODE_MT7668.bin
vendor/firmware/bt.cfg
vendor/firmware/mt7663_patch_e2_hdr.bin
vendor/firmware/mt7668_patch_e2_hdr.bin
vendor/firmware/wifi_mt7663.cfg
vendor/firmware/wifi_mt7668.cfg
vendor/firmware/woble_setting_7663.bin
vendor/firmware/woble_setting_7668.bin
vendor/framework/com.dolby.android.audio.jar
vendor/lib/firmware/dongle.cfg
vendor/lib/hw/audio.stub.default.so
vendor/lib/hw/hdmi_cec.mt5867.so
vendor/lib/hw/synchronizer.mt5867.so
vendor/lib/hw/vendor.mediatek.camera.provider@2.4-impl.so
vendor/lib/hw/vendor.mediatek.graphics.externalbuffer@1.0-impl.so
vendor/lib/utopia/audio_bin/aucode_adec_r2.bin
vendor/lib/utopia/audio_bin/aucode_adec_r2_MS12V22.bin
vendor/lib/utopia/audio_bin/aucode_asnd_r2.bin
vendor/lib/utopia/audio_bin/aucode_asnd_r2_MS12V22.bin
vendor/lib/utopia/vdec_bin/fwVPU.bin
vendor/lib/utopia/vdec_bin/fwVPU_VLC.bin
vendor/lib/libBaseLib.so
vendor/lib/libOMX.MStar.so
vendor/lib/libOpTvApiClient.so
vendor/lib/libOpenCL.so
vendor/lib/libVendorGS.so
vendor/lib/libbluetooth_mtk.so
vendor/lib/libbt-vendor.so
vendor/lib/libcapture.so
vendor/lib/libcom_mediatek_net_jni.so
vendor/lib/libcom_mediatek_twoworlds_tv_jni.so
vendor/lib/libdrmportingoptee.so
vendor/lib/libdrmportingtee.so
vendor/lib/libfactory_api.so
vendor/lib/libhashkey_ca.so
vendor/lib/libhdcp.so
vendor/lib/libhdcpCA.so
vendor/lib/libintertaca.so
vendor/lib/libliveMedia.so
vendor/lib/libplayready4.x.so
vendor/lib/libplayreadyhidl.so
vendor/lib/libsecurestorage.so
vendor/lib/libsharemem.so
vendor/lib/libteec.so.1
vendor/lib/libteec.so.1.0
vendor/lib/libvdpomx.so
vendor/lib/libvpx.so
vendor/lib/libwfd.so
vendor/lib/libwifidisplay-jni.so
vendor/lib/libwpa_client.so
vendor/lib/libxtestsupport2.so
vendor/lib/mstar.hardware.mmdisp@1.0-impl.so
vendor/lib/vendor.mediatek.camera.device@3.4-external-impl.so
vendor/lib/vendor.mediatek.camera.provider@2.4-external.so
vendor/lib/vendor.mediatek.camera.provider@2.4.so
vendor/lib/vendor.mediatek.hardware.tv.networkproxy@1.0.so
vendor/lib/vendor.mediatek.tv.Mm.Mm.mtktvfapicustom@1.0.so
vendor/lib/vendor.mediatek.tv.OneplusTvApi@1.0_vendor.so
vendor/lib/vendor.mediatek.tv.cmpb@1.0.so
vendor/lib/vendor.mediatek.tv.mtkdmservice@1.0.so
vendor/lib/vendor.mediatek.tv.mtkmtalservice@1.0.so
vendor/lib/vendor.mediatek.tv.mtktvapi@1.0-cwrapper.so
vendor/lib/vendor.mediatek.tv.mtktvapi@1.0.so
vendor/lib/vendor.mediatek.tv.mtktvapicustom@1.0-cwrapper.so
vendor/lib/vendor.mediatek.tv.mtktvapicustom@1.0-impl.so
vendor/lib/vendor.mediatek.tv.mtktvapicustom@1.0.so
vendor/lib/vendor.mediatek.tv.mtktvfactory@1.0-imp.so
vendor/lib/vendor.mediatek.tv.mtktvfactory@1.0.so
vendor/lib/vendor.mediatek.tv.mtktvfapicustom@1.0-impl.so
vendor/lib/vendor.mediatek.wifidisplay@1.0.so
vendor/odm/app/TvLauncherCustomizer2/TvLauncherCustomizer2.apk
vendor/usr/idc/Vendor_0094_Product_0001.idc
vendor/usr/idc/Vendor_2b31_Product_4852.idc
vendor/usr/idc/Vendor_2b31_Product_4857.idc
vendor/usr/idc/Vendor_2b31_Product_4858.idc
vendor/usr/idc/Vendor_2b31_Product_4859.idc
vendor/usr/idc/Vendor_2b31_Product_4860.idc
vendor/usr/idc/Vendor_2b31_Product_4870.idc
vendor/usr/idc/Vendor_2b31_Product_4871.idc
vendor/usr/idc/Vendor_2b31_Product_4872.idc
vendor/usr/idc/Vendor_2b31_Product_4873.idc
vendor/usr/idc/Vendor_2b31_Product_4874.idc
vendor/usr/idc/Vendor_2b31_Product_4875.idc
vendor/usr/idc/Vendor_2b31_Product_4880.idc
vendor/usr/idc/Vendor_2b31_Product_4890.idc
vendor/usr/idc/Vendor_2b31_Product_6891.idc
vendor/usr/idc/Vendor_3697_Product_0003.idc
vendor/usr/keylayout/Vendor_0046_Product_3838.kl
vendor/usr/keylayout/Vendor_0094_Product_0001.kl
vendor/usr/keylayout/Vendor_1d5a_Product_c580.kl
vendor/usr/keylayout/Vendor_2b31_Product_4852.kl
vendor/usr/keylayout/Vendor_2b31_Product_4857.kl
vendor/usr/keylayout/Vendor_2b31_Product_4858.kl
vendor/usr/keylayout/Vendor_2b31_Product_4859.kl
vendor/usr/keylayout/Vendor_2b31_Product_4860.kl
vendor/usr/keylayout/Vendor_2b31_Product_4870.kl
vendor/usr/keylayout/Vendor_2b31_Product_4871.kl
vendor/usr/keylayout/Vendor_2b31_Product_4872.kl
vendor/usr/keylayout/Vendor_2b31_Product_4873.kl
vendor/usr/keylayout/Vendor_2b31_Product_4874.kl
vendor/usr/keylayout/Vendor_2b31_Product_4875.kl
vendor/usr/keylayout/Vendor_2b31_Product_4880.kl
vendor/usr/keylayout/Vendor_2b31_Product_4890.kl
vendor/usr/keylayout/Vendor_2b31_Product_6891.kl
vendor/usr/keylayout/Vendor_3697_Product_0001.kl
vendor/usr/keylayout/Vendor_3697_Product_0002.kl
vendor/usr/keylayout/Vendor_3697_Product_0003.kl
vendor/usr/keylayout/ttxkeymap.ini
vendor/config
