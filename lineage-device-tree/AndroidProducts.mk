#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Dubai.mk

COMMON_LUNCH_CHOICES := \
    lineage_Dubai-user \
    lineage_Dubai-userdebug \
    lineage_Dubai-eng
